#!/bin/bash

pem_create() {
  ssh-keygen -f nginx-key-pair.pem -q -N ""
}

t_init() {
  terraform init
}

t_plan() {
  terraform plan
}

t_apply() {
  terraform apply -auto-approve
}

for ARGUMENT in "$@"
do
  KEY=$(echo $ARGUMENT | cut -f1 -d=)
  VALUE=$(echo $ARGUMENT | cut -f2 -d=)
  case "$KEY" in
    INIT)
      INIT=${VALUE} ;;
    PLAN)
      PLAN=${VALUE} ;;
    APPLY)
      APPLY=${VALUE} ;;
    *)
  esac
done

if [[ 1 = $INIT ]]
  then
  t_init
fi

if [[ 1 = $PLAN ]]
  then
  t_plan
fi

if [[ 1 = $APPLY ]]
  then
  t_apply
fi
