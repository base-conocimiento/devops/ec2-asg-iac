#!/bin/bash

sudo apt-get update
sudo apt-get -y install nginx wget openjdk-8-jdk
wget -O /app.jar https://assets-public-example.s3.amazonaws.com/app.jar
java -Xmx1G -jar /app.jar&

sudo su
echo """
server {
  listen 80 default_server;
  sendfile            on;
  tcp_nopush          on;
  tcp_nodelay         on;
  keepalive_timeout   99999;
  types_hash_max_size 99999;
  keepalive_requests 1000000;
  include             /etc/nginx/mime.types;
  default_type        application/octet-stream;
  gzip on;
  gzip_http_version 1.1;
  gzip_disable "msie6";
  gzip_min_length   1100;
  gzip_vary on;
  gzip_proxied      expired no-cache no-store private auth;
  gzip_comp_level   9;
  gzip_types text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript application/javascript;

  error_log /var/log/nginx/error.log warn;
  access_log /var/log/nginx/access.log;

  root /var/www/html;

  location / {
    proxy_pass http://127.0.0.1:8080/main/;
    proxy_redirect    off;
  }
}
""" > /etc/nginx/conf.d/default.conf

echo """
user www-data;
worker_processes auto;
pid /run/nginx.pid;
include /etc/nginx/modules-enabled/*.conf;

events {
  worker_connections 768;
}

http {
  sendfile on;
  tcp_nopush on;
  tcp_nodelay on;
  keepalive_timeout 65;
  types_hash_max_size 2048;
  include /etc/nginx/mime.types;
  default_type application/octet-stream;
  ssl_protocols TLSv1 TLSv1.1 TLSv1.2 TLSv1.3; # Dropping SSLv3, ref: POODLE
  ssl_prefer_server_ciphers on;
  access_log /var/log/nginx/access.log;
  error_log /var/log/nginx/error.log;
  gzip on;
  include /etc/nginx/conf.d/*.conf;
}
""" > /etc/nginx/nginx.conf

service nginx start
