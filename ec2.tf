module "autoscaling" {
  source  = "terraform-aws-modules/autoscaling/aws"
  version = "~> 3.0"

  name = "nginx-test"
  image_id = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  security_groups = [
    aws_security_group.allow_http.id
  ]
  load_balancers  = [
    module.elb.this_elb_id
  ]
  key_name = aws_key_pair.nginx_test_key_pair.id
  asg_name = "nginx-test-asg"
  vpc_zone_identifier = [
    #aws_subnet.test_subnet_private.id,
    aws_subnet.test_subnet_public.id
  ]
  health_check_type         = "EC2"
  min_size                  = 2
  max_size                  = 5
  desired_capacity          = 2
  wait_for_capacity_timeout = 0
  user_data = file("nginx.sh")
}

data "aws_ami" "ubuntu" {
  most_recent = true
  filter {
    name      = "name"
    values    = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name      = "virtualization-type"
    values    = ["hvm"]
  }

  owners      = ["099720109477"]
}

resource "aws_key_pair" "nginx_test_key_pair" {
    key_name = "nginx-key-pair"
    public_key = file("ssh/nginxKey.pub")
}
