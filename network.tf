resource "aws_internet_gateway" "test_igw" {
  vpc_id          = aws_vpc.test_vpc.id
  tags            = {
    Name    = "test-igw"
    Terraform   = "true"
  }
}

resource "aws_route_table" "test_crt" {
  vpc_id          = aws_vpc.test_vpc.id

  route {
    cidr_block    = "0.0.0.0/0"
    gateway_id    = aws_internet_gateway.test_igw.id
  }

  tags            = {
    Name    = "test-public-crt"
    Terraform   = "true"
  }
}

resource "aws_route_table_association" "test_crta_public_subnet"{
  subnet_id       = aws_subnet.test_subnet_public.id
  route_table_id  = aws_route_table.test_crt.id
}

resource "aws_eip" "test_elastic_ip" {
  vpc      = true
}

resource "aws_nat_gateway" "test_nat_gateway" {
  allocation_id = aws_eip.test_elastic_ip.id
  subnet_id     = aws_subnet.test_subnet_public.id

  tags = {
    Name = "test-nat-gateway"
  }
}

resource "aws_route_table" "test_nat_route_table" {
  vpc_id = aws_vpc.test_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.test_nat_gateway.id
  }

  tags = {
    Name = "test-nat-route-table"
  }
}

resource "aws_route_table_association" "associate_routetable_to_private_subnet" {
  subnet_id      = aws_subnet.test_subnet_private.id
  route_table_id = aws_route_table.test_nat_route_table.id
}

resource "aws_security_group" "allow_http" {
  name            = "allow_http"
  description     = "Allow HTTP inbound traffic"
  vpc_id          = aws_vpc.test_vpc.id

  ingress {
    description   = "HTTP from VPC"
    from_port     = 80
    to_port       = 80
    protocol      = "tcp"
    cidr_blocks   = ["0.0.0.0/0"]
  }

  egress {
    from_port     = 0
    to_port       = 0
    protocol      = "-1"
    cidr_blocks   = ["0.0.0.0/0"]
  }

  tags            = {
    Name = "allow_http"
    Terraform   = "true"
  }
}
