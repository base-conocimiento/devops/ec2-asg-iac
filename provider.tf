provider "aws" {
  region      = "us-east-1"
  profile     = "personal"
  version     = "~> 3.29.0"
}
