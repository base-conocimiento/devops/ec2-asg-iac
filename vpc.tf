resource "aws_vpc" "test_vpc" {
  cidr_block                = var.VPC_CIDR
  enable_dns_support        = "true"
  enable_dns_hostnames      = "true"
  enable_classiclink        = "false"
  instance_tenancy          = "default"
  tags                      = {
    Name      = "test_vpc"
    Terraform   = "true"
  }
}

resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
    Terraform   = "true"
  }
}

resource "aws_subnet" "test_subnet_public" {
  vpc_id                    = aws_vpc.test_vpc.id
  cidr_block                = var.SUBNET_PUBLIC
  map_public_ip_on_launch   = "true"
  availability_zone         = "us-east-1a"
  tags                      = {
    Name    = "test_subnet_public"
    Terraform   = "true"
  }
}

resource "aws_subnet" "test_subnet_private" {
  vpc_id                    = aws_vpc.test_vpc.id
  cidr_block                = var.SUBNET_PRIVATE
  map_public_ip_on_launch   = "false"
  availability_zone         = "us-east-1b"
  tags                      = {
    Name    = "test_subnet_private"
    Terraform   = "true"
  }
}
