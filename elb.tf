module "elb" {
  source  = "terraform-aws-modules/elb/aws"

  name = "elb-example"
  subnets         = [
    aws_subnet.test_subnet_private.id,
    aws_subnet.test_subnet_public.id
  ]
  security_groups = [
    aws_security_group.allow_http.id
  ]
  internal        = false

  listener = [
    {
      instance_port     = "80"
      instance_protocol = "HTTP"
      lb_port           = "80"
      lb_protocol       = "HTTP"
    }
  ]

  health_check = {
    target              = "HTTP:80/"
    interval            = 30
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 5
  }

  tags = {
    Name = "ELB Nginx test"
  }
}
