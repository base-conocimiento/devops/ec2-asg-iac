variable "VPC_CIDR" {
  default = "142.0.0.0/16"
}

variable "SUBNET_PUBLIC" {
  default = "142.0.1.0/24"
}

variable "SUBNET_PRIVATE" {
  default = "142.0.2.0/24"
}
